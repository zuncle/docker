# tomcat8-oracle-java8

* Tomcat 8.0.30 with java Oracle 1.8.0_72
* based on buildpack-deps:trusty
* with APR

see Dockerfile at [zuncle/tomcat8-oracle-java8](https://bitbucket.org/zuncle/docker/src/644d82284ce4c00a08f9eff144b3156f45cb94da/tomcat8-oracle-java8/Dockerfile)
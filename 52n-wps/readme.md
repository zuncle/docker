# 52n-wps

* 52° North WPS compiled with geotools
* Only local algorithms activated

See Dockerfile at [docker/52n-wps](https://bitbucket.org/zuncle/docker/src/644d82284ce4c00a08f9eff144b3156f45cb94da/52n-wps/Dockerfile)